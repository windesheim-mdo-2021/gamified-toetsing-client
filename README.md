# Gamified Toetsing
## Client

---

# Project structure

```text
src
|---adapters
|---components
|---styles
|---pages
```

## Adapters
Adapters are the connectors of your application with the outside world. Any form of API call or websocket interaction which needs to happen, to share data with an external service or client, should happen within the adapter itself.

## Components
Components are the life-blood of your application. They will hold the UI for your application, and can sometimes hold the Business Logic and also any State which has to be maintained.

## Contexts
Contains pages structured by routes.

## Styles
Contains stylesheets.