import React from "react";
import "../../styles/components/Generic/Card.scss";

export const Card = (props) => {
    return (
        <div className={`card ${props.className}`}>
            <div className="container">
                <div className="row">
                    <div className="col col--6">
                        {props.children}
                    </div>
                </div>
            </div>
        </div>
    )
};