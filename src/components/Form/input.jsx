import {Component} from "react";
import PropTypes from 'prop-types';
import "../../styles/components/Form/input.scss";

export default class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: this.props.name,
            value: ''
        }
    }

    handleChange = async (e) => {
        await this.setState({value: e.target.value});
        this.props.handleInput(this.state.name, this.state.value);
    };

    handleAutoSave = () => {
        if (this.props.handleAutoSave) {
            this.props.handleAutoSave();
        }
    }

    componentDidMount() {
        if(this.props.value) {
            this.setState({value: this.props.value})
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.value !== this.props.value) {
            this.setState({value: this.props.value})
        }
    }

    render() {
        return (
            <div className={`input ${this.props.className ?? ""}`}>
                <label>{this.props.label ?? this.props.name}
                    <input type={`${this.props.type ?? "text"}`}
                           value={this.state.value}
                           onChange={this.handleChange}
                           onBlur={this.handleAutoSave}/>
                </label>
            </div>
        )
    }
}

Input.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    className: PropTypes.string,
    handleInput: PropTypes.func.isRequired
};