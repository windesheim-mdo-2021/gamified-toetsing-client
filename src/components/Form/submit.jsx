import React, {Component} from "react";
import PropTypes from 'prop-types';
import "../../styles/components/Form/button.scss";

export default class Submit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            random: Math.random()
        }
    }

    render() {
        return (
            <div className={`submit button ${this.props.className ?? ""}`}>
                <button onClick={this.props.handleSubmit}>{this.props.name}</button>
            </div>
        )
    }
}

Submit.propTypes = {
    name: PropTypes.string.isRequired,
    className: PropTypes.string,
    handleSubmit: PropTypes.func.isRequired
};