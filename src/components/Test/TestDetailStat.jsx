import React, {Component} from "react";
import "../../styles/components/Test/TestDetailStat.scss";

export default class TestDetailStat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: {}
        }
    }

    render = () => {
        return (
            <div className="testDetailStat">
                <div className="value">{this.props.value}</div>
                <div className="type">{this.props.type}</div>
            </div>
        )
    }
}