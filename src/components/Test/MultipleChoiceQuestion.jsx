import React, {useEffect, useState} from "react";
import xhr from "../../adapters/xhr";
import {useParams} from "react-router-dom";
import Input from "../Form/input";
import {Card} from "../Generic/Card";
import Textarea from "../Form/textarea";
import {MultipleChoiceOption} from "./MultipleChoiceOption";

export const MultipleChoiceQuestion = (props) => {
    const [api] = useState(new xhr());
    const [question, setQuestion] = useState(null);
    const [options, setOptions] = useState([]);
    const [input, setInput] = useState([]);
    const params = useParams();

    useEffect(() => {
        const loadOptions = (optionsObj, questionObj) => {
            let vOptions = [];
            if(props.edit) {
                vOptions = options;
            }
            let jOptions = optionsObj.map((o) => {
                return (
                    <MultipleChoiceOption
                        handleOptionSelect={async (e, id) => {
                            await api.post(`test/${params.id}/answer/submit`,
                                {
                                    testId: params.id,
                                    casusId: params.casusid,
                                    questionId: questionObj.id,
                                    optionId: id,
                                });
                        }}
                        inputName={`question-${questionObj.id}`}
                        option={o}
                        key={'option-'+(Math.random())}
                        edit={props.edit}/>
                )
            });
            setOptions([...jOptions, ...vOptions]);
        }

        async function fetchData() {
            if (props.question) {
                const resQuestion = await api.get(`test/${params.id}/casus/${params.casusid}/questions/get/${props.question.id}`);
                await setQuestion(resQuestion);

                const resOptions = await api.get(`test/${params.id}/casus/questions/${props.question.id}/options/get`);
                loadOptions(resOptions, resQuestion);
            }

            const virtualInput = input;
            virtualInput['title'] = question?.title ?? "";
            virtualInput['description'] = question?.description ?? "";
            setInput(virtualInput);
        }

        fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [api, input, params.casusid, params.id, props.edit, props.question]);

    const handleInput = (key, value) => {
        const virtualInput = input;
        virtualInput[key] = value
        setInput(virtualInput);
    };

    const handleAutoSave = async () => {
        if (!input?.title) {
            return;
        }

        if (question === null) {
            const res = await api.post(`test/${params.id}/casus/question/create`, {...input, casusId: params.casusid});
            if (res) {
                setQuestion(res);
            }
        } else {
            const res = await api.post(`test/${params.id}/casus/questions/update/${question.id}`, {...input, questionId: question.id});
            if (res) {
                setQuestion(res);
            }
        }
    };

    const handleAddOption = () => {
        let vOptions = options;

        const option = (
            <MultipleChoiceOption questionId={question.id ?? false} key={'option-'+(vOptions.length + 1)} edit={props.edit}/>
        );
        vOptions.push(option);
        setOptions([...vOptions]);
    };

    if(props.edit) {
        return (
            <Card className={'question multipleChoiceQuestion'}>
                <Input value={question?.title} label={'Vraag titel'} name={'title'}
                       handleInput={handleInput} handleAutoSave={handleAutoSave}/>
                <Textarea value={question?.description ?? ""} label={'Vraag omschrijving'} name={'description'}
                          handleInput={handleInput} handleAutoSave={handleAutoSave} />
                <h3>Antwoorden</h3>
                {options}
                <div onClick={handleAddOption} className="button">
                    <span>Optie toevoegen</span>
                </div>
            </Card>
        )
    } else {
        return (
            <Card className={'question multipleChoiceQuestion'}>
                <p><strong>{question?.title}</strong></p>
                <p>{question?.description}</p>
                <h3>Antwoorden</h3>
                {options}
            </Card>
        )
    }
};