import React, {useEffect, useState} from "react";
import "../../styles/components/Test/CasusOverview.scss";
import {useHistory, useParams} from "react-router-dom";
import xhr from "../../adapters/xhr";

export const CasusOverview = (props) => {
    const [api] = useState(new xhr());
    const [casussen, setCasussen] = useState(null);
    const params = useParams();
    const history = useHistory();
    
    const goBack = () => {
        history.push(`/toetsen/edit/${params.id}`);
    }

    const addCasus = () => {
        history.push(`/toetsen/edit/${params.id}/casus/create`);
    }

    const submitTest = () => {
        alert("NOT IMPLEMENTED SUBMITTEST")
    }

    useEffect(() => {
        const handleSelectCasus = (id) => {
            if (props.edit) {
                history.push(`/toetsen/edit/${params.id}/casus/${id}/edit`);
            } else {
                history.push(`/toets/${params.id}/casus/${id}`);
            }
        }

        const buildCasus = (items) => {
            if (items === null || items.length === 0) {
                return setCasussen([]);
            }

            const elements = items.map(c => {
                return (
                    <div className={'casusEntry'} onClick={() => handleSelectCasus(c.id)}>
                        <div className="inner">
                            <h3>{c.title}</h3>
                        </div>
                    </div>
                )
            });

            setCasussen(elements);
        };

        async function fetchData() {
            const res = await api.get(`test/${params.id}/casus`);
            buildCasus(res);
        }
        fetchData();
    }, [api, params.id, props.edit, history]);



    if(props.edit) {
        return (
            <div className="casusOverview">
                <div className="casusList">
                    <div className="container">
                        <div className="row">
                            <div className="col col--6">
                                {casussen}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="casusControls">
                    <div>
                        <button onClick={addCasus}>Casus toevoegen</button>
                    </div>
                    <div>
                        <button onClick={goBack}>Opslaan en terug</button>
                    </div>
                </div>
            </div>
        )
    } else {
        return (
            <div className="casusOverview">
                <div className="casusList">
                    <div className="container">
                        <div className="row">
                            <div className="col col--6">
                                {casussen}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="casusControls">
                    <div>
                        <button onClick={submitTest}>Toets inleveren</button>
                    </div>
                </div>
            </div>
        )
    }
};