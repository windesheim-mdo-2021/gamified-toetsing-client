import React, {useEffect, useState} from "react";
import xhr from "../../adapters/xhr";
import {useParams} from "react-router-dom";
import Input from "../Form/input";
import {Card} from "../Generic/Card";

export const MultipleChoiceOption = (props) => {
    const [api] = useState(new xhr());
    const [option, setOption] = useState(null);
    const [answer, setAnswer] = useState("");
    const [input, setInput] = useState([]);
    const params = useParams();

    useEffect(() => {
        async function fetchData() {
            if (props.option) {
                const resOption = await api.get(`test/${params.id}/casus/questions/${props.option.question_id}/option/get/${props.option.id}`);
                await setOption(resOption);

                getAnswer();

                const virtualInput = input;
                virtualInput['answer'] = resOption?.answer ?? "";
                virtualInput['score'] = resOption?.score ?? "";
                setInput(virtualInput);
            }

        }

        fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [api, input, props.edit, params.id]);

    const getAnswer = async () => {
        const resAnswer = await api.get(`test/${params.id}/answer/check/${props.option.id}`);
        if(resAnswer) {
            setAnswer("checked");
        }
    }

    const handleInput = (key, value) => {
        const virtualInput = input;
        virtualInput[key] = value
        setInput(virtualInput);
    };

    const handleAutoSave = async () => {
        if (!input?.answer) {
            console.log("RETOUR", input)
            return;
        }

        if (option === null) {
            const res = await api.post(`test/${params.id}/casus/questions/${props.questionId}/option/create`, {...input, questionId: props.questionId});
            if (res) {
                setOption(res);
            }
        } else {
            const res = await api.post(`test/${params.id}/casus/questions/${props.questionId}/option/update/${option.id}`, {...input, optionId: option.id});
            if (res) {
                setOption(res);
            }
        }
    };

    const handleOptionSelect = async (e) => {
        await props.handleOptionSelect(e, option.id);
        getAnswer();
    }

    if(props.edit) {
        return (
            <Card className={'option multipleChoiceOption'}>
                <Input value={option?.answer} label={'Antwoord'} name={'answer'}
                       handleInput={handleInput} handleAutoSave={handleAutoSave}/>
                <Input value={option?.score} label={'Score'} name={'score'}
                       type={'number'}
                       handleInput={handleInput} handleAutoSave={handleAutoSave}/>
            </Card>
        )
    } else {
        if(option) {
            return (
                <div className={'option multipleChoiceOption input'}>
                    <input checked={answer}
                           onChange={(e) => handleOptionSelect(e)}
                           type="radio"
                           name={props.inputName}
                           id={`answer-${option.id}`}
                           value={option.id}/>
                    <label htmlFor={`answer-${option.id}`}>{option.answer}</label>
                </div>
            )
        } else {
            return "";
        }
    }
};