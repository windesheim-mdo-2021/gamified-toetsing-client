import React, {Component} from "react";
import TestDetailStat from "./TestDetailStat";
import Input from "../Form/input";
import Textarea from "../Form/textarea";
import Submit from "../Form/submit";

export default class TestDetailInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: {}
        }
    }

    handleInput = async (key, value) => {
        const input = await this.state.input;
        input[key] = value;
        await this.setState(input);
    };

    handleSubmit = async () => {
        this.props.callback(this.state.input);
    };

    render = () => {
        return (
            <div className="container">
                <div className="row">
                    <div className="col col--6">
                        <TestDetailStat value={0} type={'Casussen'}/>
                        <TestDetailStat value={0} type={'Vragen'}/>
                    </div>
                    <div className="col col--6">
                        {this.props.edit ?
                            <Input value={this.props.content?.time} label={'Tijd'} name={'time'}
                                   handleInput={this.handleInput} className={'testInput'}/>
                            :
                            <p><strong>Tijd</strong><br/> {this.props.content?.time}</p>
                        }
                        {this.props.edit ?
                            <div>
                                <Textarea value={this.props.content?.instructions} label={'Instructies'} name={'instructions'} handleInput={this.handleInput} className={'testInput'}/>
                                <Submit name={'Opslaan'} handleSubmit={this.handleSubmit}/>
                            </div>
                            :
                            <p><strong>Instructies</strong><br/> {this.props.content?.instructions}</p>
                        }
                    </div>
                </div>
            </div>
        )
    }
}