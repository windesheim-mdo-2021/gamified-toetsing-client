import React, {useEffect, useState} from "react";
import "../../styles/components/Test/Casus.scss";
import {useHistory, useParams} from "react-router-dom";
import {Card} from "../Generic/Card";
import Textarea from "../Form/textarea";
import Input from "../Form/input";
import xhr from "../../adapters/xhr";
import {MultipleChoiceQuestion} from "./MultipleChoiceQuestion";

export const Casus = (props) => {
    const [api] = useState(new xhr());
    const [casus, setCasus] = useState(null);
    const [input, setInput] = useState([]);
    const [questions, setQuestions] = useState([]);
    const params = useParams();
    const history = useHistory();

    useEffect(() => {
        const loadQuestions = (questionsObj) => {
            let vQuestions = questions;

            let jQuestions = [];
            if (questionsObj) {
                jQuestions = questionsObj.map((q) => {
                    return (
                        <MultipleChoiceQuestion key={'question-'+(Math.random())} question={q} edit={props.edit}/>
                    )
                });
            }
            setQuestions([...jQuestions, ...vQuestions]);
        }

        async function fetchData() {
            const res = await api.get(`test/${params.id}/casus/${params.casusid}`);
            await setCasus(res);

            const resQuestions = await api.get(`test/${params.id}/casus/${params.casusid}/questions/get`);
            loadQuestions(resQuestions);

            if(res) {
                const virtualInput = input;
                virtualInput['title'] = res.title;
                virtualInput['description'] = res.description;
                setInput(virtualInput);
            }
        }

        fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [api, input, params.casusid, params.id, props.edit]);

    const handleInput = (key, value) => {
        const virtualInput = input;
        virtualInput[key] = value
        setInput(virtualInput);
    };

    const handleAutoSave = async () => {
        if (!input?.title) {
            return;
        }

        if (casus === null || casus === undefined) {
            const res = await api.post(`test/${params.id}/casus/create`, {...input, testId: params.id});
            if (res) {
                history.push(`/toetsen/edit/${params.id}/casus/${res.id}/edit`);
            }
        } else {
            await api.post(`test/${params.id}/casus/${casus.id}/update`, {...input, testId: params.id, casusId: casus.id});
        }
    };

    const handleNewQuestion = () => {
        let vQuestions = questions;

        const question = (
            <MultipleChoiceQuestion key={'question-'+(vQuestions.length + 1)} edit={props.edit}/>
        );
        vQuestions.push(question);
        setQuestions([...vQuestions]);
    }

    return (
        <div className="casus">
            <header>
                <div className="container">
                    <div className="row">
                        <div className="col col--6">
                            <div className="goBack" onClick={() => history.goBack()}>Terug</div>
                            <h1>{casus?.title ?? "Casus"}</h1>
                        </div>
                    </div>
                </div>
            </header>
            <div className="inner">
                <Card>
                    {props.edit ?
                        <div>
                            <Input value={casus?.title} label={'Casus titel'} name={'title'}
                                   handleInput={handleInput} handleAutoSave={handleAutoSave}/>
                            <Textarea value={casus?.description ?? ""} label={'Casus omschrijving'} name={'description'}
                                      handleInput={handleInput} handleAutoSave={handleAutoSave} />
                        </div>
                        :
                        <div>
                            <h2>{casus?.title}</h2>
                            <p>{casus?.description}</p>
                        </div>
                    }
                </Card>
                {questions}
            </div>
            {props.edit ?
                <div onClick={handleNewQuestion} className="footerButton">
                    <span>Vraag toevoegen</span>
                </div>
                :
                null
            }
        </div>
    )
};