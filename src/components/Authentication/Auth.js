import React, {Component} from "react";
import xhr from "../../adapters/xhr";
import LoginPage from "../../pages/User/LoginPage";
import {Redirect} from "react-router-dom";

export default class Auth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            api: new xhr(),
            authenticated: null
        };
    }

    handleLogin = async (username, password) => {
        await this.state.api.post('user/login', {username, password});

        //@todo: handle errors.

        await this.verifyUser()
    };

    verifyUser = async () => {
        const token = await this.state.api.token();

        if (token) {
            const res = await this.state.api.post('user/verify', {});
            if(res && res.hasOwnProperty('user')) {
                return this.setState({authenticated: true}, this.handleRedirect)
            }
        }

        this.setState({authenticated: false})
    };

    handleRedirect = async () => {
        if (this.state.authenticated && this.props.redirect) {
            await this.setState({ redirect: this.props.redirect });
        }
    };

    componentWillMount = async () => {
        await this.verifyUser();
    };

    render() {
        // If state has not been computed yet,
        // don't load anything to prevent login screen flashing.
        if (this.state.authenticated == null) {
            return null;
        }

        // If user is authenticated and Auth has a redirect defined,
        // redirect the user to the page.
        if (this.state.authenticated && this.state.redirect) {
            return (
                <Redirect to={this.state.redirect} />
            );
        }

        // Render the loginpage, or Auth's children based on log-in state.
        return (
            <div>
                {this.state.authenticated ? this.props.children : <LoginPage callback={this.handleLogin}/>}
            </div>
        );
    }
}