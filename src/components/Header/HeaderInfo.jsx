import "../../styles/components/Header/HeaderInfo.scss";

export const HeaderInfo = (props) => {
    if (props.topPartOnly) {
        return (
            <header className={'info'}>
                <div className="top">
                    <div className="container">
                        <div className="row">
                            <div className="col col--6">
                                <h1>{props.title}</h1>
                                <div className="subtitle">{props.subtitle}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
    return (
        <header className={'info'}>
            <div className="top"/>
            <div className="bottom">
                <div className="container">
                    <div className="row">
                        <div className="col col--4">
                            <h1>{props.title}</h1>
                            <div className="subtitle">{props.subtitle}</div>
                        </div>
                        <div className="col col--2">
                            <button onClick={props.buttonCallback}>{props.buttonText}</button>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    )
}