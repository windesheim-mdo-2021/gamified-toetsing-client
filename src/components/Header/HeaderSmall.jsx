import {Component} from "react";
import "../../styles/components/Header/HeaderSmall.scss";

export default class HeaderBasic extends Component {
    render() {
        return (
            <header className={'small'}>
                <div className="container">
                    <div className="row">
                        <div className="col col--6">
                            <h1>{this.props.title}</h1>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}