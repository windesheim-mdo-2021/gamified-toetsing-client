import {Component} from "react";
import "../../styles/components/Header/HeaderBasic.scss";

export default class HeaderBasic extends Component {
    render() {
        return (
            <header className={'basic'}>
                <div className="container">
                    <div className="row">
                        <div className="col col--6">
                            <div className="push push--down">
                                <h1>{this.props.title}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}