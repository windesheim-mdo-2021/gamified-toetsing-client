import React, {useState} from "react";
import {HeaderInfo} from "../../components/Header/HeaderInfo";
import TestDetailInput from "../../components/Test/TestDetailInput";
import xhr from "../../adapters/xhr";
import {useHistory} from "react-router-dom";

export const CreateTestPage = () => {
    const [api] = useState(new xhr());
    const history = useHistory();

    const saveTest = async (params) => {
        const res = await api.post('test/create', params);
        if(res !== false) {
            history.push("/toetsen/edit/" + res.id);
        }
    };

    return (
        <div>
            <HeaderInfo title={"Toets 1"} subtitle={"Maak een toets"} />
            <TestDetailInput edit={true} callback={saveTest}/>
        </div>
    )
};