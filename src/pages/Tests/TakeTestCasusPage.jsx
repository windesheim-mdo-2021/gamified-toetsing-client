import React from "react";
// import xhr from "../../adapters/xhr";
import {useParams, useHistory} from "react-router-dom";
import {Casus} from "../../components/Test/Casus";

export const TakeTestCasusPage = () => {
    const params = useParams();
    const history = useHistory();

    if (!params.id || isNaN(parseInt(params.id))) {
        history.push('/toetsen');
    }

    return (
        <Casus edit={false} casus={false}/>
    )
};