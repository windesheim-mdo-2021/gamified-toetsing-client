import React from "react";
import HeaderSmall from "../../components/Header/HeaderSmall";
import {useParams, useHistory} from "react-router-dom";
import {CasusOverview} from "../../components/Test/CasusOverview";

export const TakeTestCasusOverviewPage = () => {
    const params = useParams();
    const history = useHistory();

    if (!params.id || isNaN(parseInt(params.id))) {
        history.push('/dashboard');
    }

    return (
        <div className={'flexLayout'}>
            <HeaderSmall title={"Toets 1"} />
            <CasusOverview edit={false}/>
        </div>
    )
};