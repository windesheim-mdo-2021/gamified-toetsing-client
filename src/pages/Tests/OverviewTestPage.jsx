import React, {useEffect, useState} from "react";
import {HeaderInfo} from "../../components/Header/HeaderInfo";
import xhr from "../../adapters/xhr";
import {Link} from "react-router-dom";

export const OverviewTestPage = (props) => {
    const [api] = useState(new xhr());
    const [tests, setTests] = useState([]);

    useEffect(() => {
        async function fetchData() {
            api.get('test')
                .then((t) => {
                    const overview = t.map((test) => {
                        console.log(test)
                        return <div>
                                    <Link to={`/toetsen/edit/${test.id}`}>
                                        {test.instructions}
                                    </Link>
                                </div>
                    })
                    setTests(overview);
                })
        }
        fetchData();
    }, [api]);

    return (
        <div>
            <HeaderInfo
                topPartOnly={true}
                title={"Toetsen"}
                subtitle={""}
            />
            <div className="adminControls">
                <div className="container">
                    <div className="row">
                        <div className="col col--6">
                            <Link to={'/toetsen/create'}>Nieuwe toets maken</Link>
                            {tests}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}