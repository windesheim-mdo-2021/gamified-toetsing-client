import React from "react";
import {useParams, useHistory} from "react-router-dom";
import {Casus} from "../../components/Test/Casus";

export const EditCasusPage = () => {
    const params = useParams();
    const history = useHistory();

    if (!params.id || isNaN(parseInt(params.id))) {
        history.push('/toetsen');
    }

    return (
        <Casus edit={true} casus={false}/>
    )
};