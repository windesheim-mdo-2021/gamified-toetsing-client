import React, {useEffect, useState} from "react";
import {HeaderInfo} from "../../components/Header/HeaderInfo";
import TestDetailInput from "../../components/Test/TestDetailInput";
import xhr from "../../adapters/xhr";
import {useParams, useHistory} from "react-router-dom";

export const TakeTestPage = () => {
    const [api] = useState(new xhr());
    const [test, setTest] = useState(null);
    const params = useParams();
    const history = useHistory();

    if (!params.id) {
        history.push('/dashboard');
    }

    const takeTest = () => {
        history.push(`/toets/${params.id}/casus`);
    }

    useEffect(() => {
        async function fetchData() {
            setTest(await api.get('test/' + params.id));
        }
        fetchData();
    }, [api, params]);

    return (
        <div>
            <HeaderInfo
                title={"Toets 1"}
                subtitle={"n/a"}
                buttonText={"Start toets"}
                buttonCallback={takeTest}
            />
            <TestDetailInput edit={false} time={test?.time} content={test}/>
        </div>
    )
};