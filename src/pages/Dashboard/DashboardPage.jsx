import React, {useEffect, useState} from "react";
import {HeaderInfo} from "../../components/Header/HeaderInfo";
import {Link} from "react-router-dom";
import xhr from "../../adapters/xhr";

export const DashboardPage = () => {
    const [api] = useState(new xhr());
    const [tests, setTests] = useState([]);

    useEffect(() => {
        async function fetchData() {
            api.get('test')
                .then((t) => {
                    const overview = t.map((test) => {
                        return (
                                <Link to={`/toets/${test.id}`}>
                                    <div className="generalButton">
                                        <span className="btn">{test.instructions} <span className="tip">Start!</span></span>
                                    </div>
                                </Link>
                            )
                    })
                    setTests(overview);
                })
        }
        fetchData();
    }, [api]);

    return (
        <div>
            <HeaderInfo
                topPartOnly={true}
                title={"Welkom"}
                subtitle={"n/a"}
            />
            <div className="testControls">
                <div className="container">
                    <div className="row">
                        <div className="col col--6">
                            <h2>Toetsen</h2>
                            {tests}
                        </div>
                    </div>
                </div>
            </div>
            <div className="adminControls">
                <div className="container">
                    <div className="row">
                        <div className="col col--6">
                            <h2>Tools</h2>
                            <Link to={'/toetsen'}>
                                <div className="generalButton">
                                    <span className="btn">Toetsen <span className="tip">Ga!</span></span>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}