import React, {Component} from "react";

import HeaderBasic from "../../components/Header/HeaderBasic";
import Input from "../../components/Form/input";
import Submit from "../../components/Form/submit";

export default class LoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            input: {}
        }
    }

    handleInput = async (key, value) => {
        const input = await this.state.input;
        input[key] = value;
        await this.setState(input);
    };

    handleSubmit = async () => {
        this.props.callback(this.state.input.username, this.state.input.password);
    };

    render = () => {
        return (
            <div>
                <HeaderBasic title={'Inloggen'}/>
                <div className={'container'}>
                    <div className="row">
                        <div className="col col--6">
                            <div className="form">
                                <Input className={'loginStyle'} label={'Gebruikersnaam'} name={'username'} handleInput={this.handleInput}/>
                                <Input className={'loginStyle'} label={'Wachtwoord'} name={'password'} type={'password'}
                                       handleInput={this.handleInput}/>
                                <Submit className={'loginStyle'} name={'Inloggen'} handleSubmit={this.handleSubmit}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}