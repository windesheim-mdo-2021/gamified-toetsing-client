const { REACT_APP_API_ENDPOINT } = process.env;

export default class xhr {
    constructor() {
        this.source = REACT_APP_API_ENDPOINT;
    }

    token = async (token = null) => {
        if(token !== null) {
            return localStorage.setItem("jwt", token);
        }
        return localStorage.getItem("jwt");
    };

    refreshToken = async (token = null) => {
        if(token !== null) {
            return localStorage.setItem("jwt_refresh", token);
        }
        return localStorage.getItem("jwt_refresh");
    };

    getAuth = async () => {
        const jwt = await this.token();
        if(jwt === null) {
            return new Headers();
        }

        return new Headers({
            Authorization: `Bearer ${jwt}`,
        });
    };

    handleResponse = async (response) => {
        if (!response) {
            return null;
        }
        if(response.hasOwnProperty('error')) {
            throw new Error("API error: " + response.error)
        }

        if(
            response.hasOwnProperty('accessToken')
            && response.hasOwnProperty('refreshToken')
        ) {
            this.token(response.accessToken);
            this.refreshToken(response.refreshToken);
            console.info("Wrote new tokens to client.")
        }

        return response;
    };

    get = async (route) => {
        return fetch(`${this.source}/${route}`,
            {
                method: 'get',
                headers: await this.getAuth(),
            })
            .then((res) => res.json())
            .then((res) => this.handleResponse(res))

            .catch(console.error);
    };

    post = async (route, vars) => {
        const headers = await this.getAuth();
        headers.append('Content-Type', 'application/json');
        return fetch(`${this.source}/${route}`,
            {
                method: 'post',
                headers: headers,
                body: JSON.stringify(vars)
            })
            .then((res) => res.json())
            .then((res) => this.handleResponse(res))
            .catch(console.error);
    }
}