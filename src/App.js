import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import './styles/app.scss';
import Auth from "./components/Authentication/Auth";
import LoginPage from "./pages/User/LoginPage";
import {DashboardPage} from "./pages/Dashboard/DashboardPage";
import {CreateTestPage} from "./pages/Tests/CreateTestPage";
import {OverviewTestPage} from "./pages/Tests/OverviewTestPage";
import {EditTestPage} from "./pages/Tests/EditTestPage";
import {EditCasusOverviewPage} from "./pages/Tests/EditCasusOverviewPage";
import {EditCasusPage} from "./pages/Tests/EditCasusPage";
import {TakeTestPage} from "./pages/Tests/TakeTestPage";
import {TakeTestCasusOverviewPage} from "./pages/Tests/TakeTestCasusOverviewPage";
import {TakeTestCasusPage} from "./pages/Tests/TakeTestCasusPage";

function App() {
  return (
      <Router>
          <Switch>
              {/*Routes for tests*/}
              <Route path="/toets/:id/casus/:casusid"><Auth><TakeTestCasusPage/></Auth></Route>
              <Route path="/toets/:id/casus"><Auth><TakeTestCasusOverviewPage/></Auth></Route>
              <Route path="/toets/:id"><Auth><TakeTestPage/></Auth></Route>


              <Route path="/toetsen/edit/:id/casus/:casusid/edit"><Auth><EditCasusPage edit={true}/></Auth></Route>
              <Route path="/toetsen/edit/:id/casus/create"><Auth><EditCasusPage edit={false}/></Auth></Route>
              <Route path="/toetsen/edit/:id/casus"><Auth><EditCasusOverviewPage edit={true}/></Auth></Route>
              <Route path="/toetsen/edit/:id"><Auth><EditTestPage edit={true}/></Auth></Route>
              <Route path="/toetsen/create"><Auth><CreateTestPage/></Auth></Route>
              <Route path="/toetsen"><Auth><OverviewTestPage/></Auth></Route>

              <Route path="/dashboard"><Auth><DashboardPage/></Auth></Route>
              <Route path="/"><Auth redirect={'dashboard'}><LoginPage/></Auth></Route>
          </Switch>
      </Router>
  );
}

export default App;
